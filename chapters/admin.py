from django.contrib import admin
from chapters.models import Chapter

# Register your models here.
admin.site.register(Chapter)