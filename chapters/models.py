# from django.db import models
from djongo import models
from series.models import Series
from volumes.models import Volume
from persons.models import Person

# Create your models here.
class Chapter(models.Model):
	number = models.IntegerField(default=None)
	title = models.CharField(max_length=255, blank=True)
	cover_link = models.CharField(max_length=255, default=None, blank=True)
	series = models.ForeignKey(Series, on_delete=models.CASCADE, default=None)
	volume = models.ForeignKey(Volume, on_delete=models.CASCADE, default=None, blank=True, null=True)
	artists = models.ManyToManyField(Person, related_name='chapter_artists', blank=True)
	writers = models.ManyToManyField(Person, related_name='chapter_writers', blank=True)

	def __str__(self):
		if self.title:
			return str(self.series) + ' #' + str(self.number) + ': ' + self.title
		else:
			return str(self.series) + ' #' + str(self.number)

	@property
	def get_cover_link(self):
		if self.cover_link:
			return self.cover_link
		elif self.volume.cover_link:
			return self.volume.cover_link
		else:
			return self.series.get_cover_link()

	def get_critic_score(self):
		if len(self.criticchapterreview_set.all()):
			critic_score = 0.0
			for critic_review in self.criticchapterreview_set.all():
				critic_score += critic_review.score
			return int(critic_score / len(self.criticchapterreview_set.all()))
		else:
 			return None

	def get_user_score(self):
		if len(self.userchapterreview_set.all()):
			user_score = 0.0
			for user_review in self.userchapterreview_set.all():
				user_score += user_review.score
			return int(user_score / len(self.userchapterreview_set.all()))
		else:
			return None

	def get_total_score(self):
		critic_score = self.get_critic_score()
		user_score = self.get_user_score()
		return int((critic_score + user_score) / 2)
