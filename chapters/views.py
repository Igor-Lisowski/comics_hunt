from django.shortcuts import render
from chapters.models import Chapter

# Create your views here.
def show_chapters(request):
	chapters = reversed(Chapter.objects.all())
	return render(request, 'show_chapters.html', {'chapters': chapters})

def chapter(request, id):
	chapter = Chapter.objects.get(id=id)
	cover_link = chapter.get_cover_link
	critic_score = chapter.get_critic_score()
	user_score = chapter.get_user_score()

	total_score = 'NR'
	if user_score == None and critic_score == None:
		user_score = 'NR'
		critic_score = 'NR'
	elif user_score == None:
		user_score = 'NR'
	elif critic_score == None:
		critic_score = 'NR'
	else:
		total_score = chapter.get_total_score()

	return render(request, 'chapter.html', {'chapter': chapter, 'cover_link': cover_link, 'critic_score': critic_score, 'user_score': user_score, 'total_score': total_score})
