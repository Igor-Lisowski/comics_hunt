from django.shortcuts import render
from volumes.models import Volume

# Create your views here.
def show_volumes(request):
	volumes = reversed(Volume.objects.all())
	return render(request, 'show_volumes.html', {'volumes': volumes})

def volume(request, id):
	volume = Volume.objects.get(id=id)
	critic_score = volume.get_critic_score()
	user_score = volume.get_user_score()
	chapters  = volume.chapter_set.all()

	total_score = 'NR'
	if user_score == None and critic_score == None:
		user_score = 'NR'
		critic_score = 'NR'
	elif user_score == None:
		user_score = 'NR'
	elif critic_score == None:
		critic_score = 'NR'
	else:
		total_score = volume.get_total_score()

	return render(request, 'volume.html', {'volume': volume, 'critic_score': critic_score, 'user_score': user_score, 'total_score': total_score, 'chapters': chapters})
