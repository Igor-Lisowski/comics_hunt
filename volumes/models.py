from djongo import models
from series.models import Series

# Create your models here.
class Volume(models.Model):
	number = models.IntegerField()
	cover_link = models.CharField(max_length=255, default=None)
	release_date = models.DateField(default=None)
	series = models.ForeignKey(Series, on_delete=models.CASCADE, default=None)
	
	def __str__(self):
		return str(self.series.title) + ' ' + str(self.number)
	
	def get_critic_score(self):
		if len(self.criticvolumereview_set.all()):
			critic_score = 0.0
			for critic_review in self.criticvolumereview_set.all():
				critic_score += critic_review.score
			return int(critic_score / len(self.criticvolumereview_set.all()))
		else:
			return None

	def get_user_score(self):
		if len(self.uservolumereview_set.all()):
			user_score = 0.0
			for user_review in self.uservolumereview_set.all():
				user_score += user_review.score
			return int(user_score / len(self.uservolumereview_set.all()))
		else:
			return None

	def get_total_score(self):
		critic_score = self.get_critic_score()
		user_score = self.get_user_score()
		return int((critic_score + user_score) / 2)

	@property
	def get_genres(self):
		genres = self.series.genres.all()
		return ', '.join([g['name'] for g in list(genres.values('name'))])

	@property
	def get_chapters_number(self):
		chapters = self.chapter_set.all()
		return len(chapters)
	