from django.contrib import admin
from volumes.models import Volume

# Register your models here.
admin.site.register(Volume)
