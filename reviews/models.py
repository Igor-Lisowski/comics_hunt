from djongo import models
from chapters.models import Chapter
from volumes.models import Volume
from users.models import User

# Create your models here.
class Critic(models.Model):
	name = models.CharField(max_length=255)
	surname = models.CharField(max_length=255, default=None, blank=True)

	def __str__(self):
		if self.surname:
			return self.name + ' ' + self.surname
		else:
			return self.name

class Review(models.Model):
	date = models.DateField()
	text = models.TextField()
	score = models.IntegerField(default=None, blank=True)

	class Meta:
		abstract = True

class CriticReview(models.Model):
	critic = models.ForeignKey(Critic, on_delete=models.CASCADE, default=None, blank=True)

	def __str__(self):
		if self.critic.surname:
			if hasattr(self, 'volumes') and self.volumes.volume:
				return self.critic.name + ' ' + self.critic.surname + ' ' + self.volumes.volume.title + ' review'
			else:
				return self.critic.name + ' ' + self.critic.surname + ' review'
		else:
			if hasattr(self, 'volumes') and self.volumes.volume:
				return self.critic.name + ' ' + self.volumes.volume.title + ' review'
			return self.critic.name + ' review'

	class Meta:
		abstract = True

class UserReview(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, blank=True)

	def __str__(self):
		return self.user.nickname + ' review'

	class Meta:
		abstract = True

class ChapterReview(models.Model):
	chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE, default=None)

	class Meta:
		abstract = True

class VolumeReview(models.Model):
	volume = models.ForeignKey(Volume, on_delete=models.CASCADE, default=None)

	class Meta:
		abstract = True

class CriticChapterReview(Review, CriticReview, ChapterReview):
	pass

class CriticVolumeReview(Review, CriticReview, VolumeReview):
	pass

class UserChapterReview(Review, UserReview, ChapterReview):
	pass

class UserVolumeReview(Review, UserReview, VolumeReview):
	pass
	