from django.contrib import admin
from reviews.models import Critic, CriticChapterReview, CriticVolumeReview, UserChapterReview, UserVolumeReview

# Register your models here.
admin.site.register(Critic)
admin.site.register(CriticChapterReview)
admin.site.register(CriticVolumeReview)
admin.site.register(UserChapterReview)
admin.site.register(UserVolumeReview)
