from djongo import models
from genres.models import Genre

# Create your models here.
class Series(models.Model):
	title = models.CharField(max_length=255)
	genres = models.ManyToManyField(Genre, related_name='series_genres', blank=True)

	def __str__(self):
		return self.title
		
	@property
	def get_cover_link(self):
		volumes = self.volume_set.all()
		return volumes.last().cover_link

	def get_total_score(self):
		return int((self.get_critic_score() + self.get_user_score()) / 2)

	def get_critic_score(self):
		volumes_critic_score = self.get_volumes_critic_score()
		chapters_critic_score = self.get_chapters_critic_score()
		critic_score = None

		if chapters_critic_score != None and volumes_critic_score != None:
			chapters_weight = 1
			volumes_weight = float((chapters_number - chapters_without_volume_number) / chapters_number)
			critic_score = int((volumes_weight * volumes_critic_score + chapters_weight * chapters_critic_score) / (chapters_weight + volumes_weight))
		elif chapters_critic_score != None:
			critic_score = int(chapters_critic_score)
		elif volumes_critic_score != None:
			critic_score = int(volumes_critic_score)
		
		return critic_score

	def get_volumes_critic_score(self):
		not_rated_volumes_number = 0
		volumes = self.volume_set.all()
		cumulated_score = 0
		for volume in volumes:
			critic_score = volume.get_critic_score()
			if critic_score == None:
				not_rated_volumes_number += 1
			else:
				cumulated_score += critic_score

		if len(volumes) - not_rated_volumes_number:
			score = cumulated_score / (len(volumes) - not_rated_volumes_number)
			return score

		return None

	def get_chapters_critic_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_set.all()
		cumulated_score = 0
		for chapter in chapters:
			critic_score = chapter.get_critic_score()
			if critic_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += critic_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	def get_user_score(self):
		volumes_user_score = self.get_volumes_user_score()
		chapters_user_score = self.get_chapters_user_score()
		user_score = None

		if volumes_user_score != None and chapters_user_score != None:
			chapters_weight = 1
			volumes_weight = float((chapters_number - chapters_without_volume_number) / chapters_number)
			user_score = int((volumes_weight * volumes_user_score + chapters_weight * chapters_user_score) / (chapters_weight + volumes_weight))
		elif volumes_user_score != None:
			user_score = int(volumes_user_score)
		elif chapters_user_score != None:
			user_score = int(chapters_user_score)

		return user_score

	def get_volumes_user_score(self):
		not_rated_volumes_number = 0
		volumes = self.volume_set.all()
		cumulated_score = 0
		for volume in volumes:
			user_score = volume.get_user_score()
			if user_score == None:
				not_rated_volumes_number += 1
			else:
				cumulated_score += user_score

		if len(volumes) - not_rated_volumes_number > 0:
			score = cumulated_score / (len(volumes) - not_rated_volumes_number)
			return score

		return None

	def get_chapters_user_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_set.all()
		cumulated_score = 0
		for chapter in chapters:
			user_score = chapter.get_user_score()
			if user_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += user_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	def get_chapters_without_volume_number(self):
		chapters = self.chapter_set.all()
		chapters_without_volume_number = 0
		for chapter in chapters:
			if not chapter.volume:
				chapters_without_volume_number += 1
		return chapters_without_volume_number
		
	@property
	def get_genres(self):
		genres = self.genres.all()
		return ', '.join([g['name'] for g in list(genres.values('name'))])

	@property
	def get_volumes_number(self):
		volumes = self.volume_set.all()
		return len(volumes)
	

	@property
	def get_chapters_number(self):
		chapters = self.chapter_set.all()
		return len(chapters)
