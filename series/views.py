from django.shortcuts import render
from series.models import Series

# Create your views here.
def show_series(request):
	all_series = Series.objects.all()
	return render(request, 'show_series.html', {'all_series': all_series})

def series(request, id):
	series = Series.objects.get(id=id)
	critic_score = series.get_critic_score()
	user_score = series.get_user_score()

	total_score = 'NR'
	if user_score == None and critic_score == None:
		user_score = 'NR'
		critic_score = 'NR'
	elif user_score == None:
		user_score = 'NR'
		total_score = critic_score
	elif critic_score == None:
		critic_score = 'NR'
		total_score = user_score
	else:
		total_score = series.get_total_score()

	volumes = reversed(series.volume_set.all())
	chapters = reversed(series.chapter_set.all())

	return render(request, 'series.html', {'series': series, 'critic_score': critic_score, 'user_score': user_score, 'total_score': total_score, 'volumes': volumes, 'chapters': chapters})
