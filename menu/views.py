from django.shortcuts import render
from chapters.models import Chapter
from volumes.models import Volume

# Create your views here.
def show_menu(request):
	volumes = reversed(Volume.objects.all()[:6])
	chapters = reversed(Chapter.objects.all()[:10])
	return render(request, 'menu.html', {'volumes': volumes, 'chapters': chapters})
