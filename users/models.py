from djongo import models

# Create your models here.
class User(models.Model):
	nickname = models.CharField(max_length=255)
	mail = models.CharField(max_length=255)

	def __str__(self):
		return self.nickname