"""comics_hunt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from chapters.views import show_chapters, chapter
from persons.views import show_persons, person
from series.views import show_series, series
from volumes.views import show_volumes, volume
from menu.views import show_menu

urlpatterns = [
    path('', show_menu),
    path('admin/', admin.site.urls),
    path('chapters', show_chapters),
    path('chapters/<int:id>', chapter, name='chapter'),
    path('persons', show_persons),
    path('persons/<int:id>', person, name='person'),
    path('series', show_series),
    path('series/<int:id>', series, name='series'),
    path('volumes', show_volumes),
    path('volumes/<int:id>', volume, name='volume')
]
