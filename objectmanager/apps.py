from django.apps import AppConfig


class ObjectmanagerConfig(AppConfig):
    name = 'objectmanager'
