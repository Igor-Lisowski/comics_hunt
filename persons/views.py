from django.shortcuts import render
from persons.models import Person

# Create your views here.
def show_persons(request):
	persons = reversed(Person.objects.all())
	return render(request, 'show_persons.html', {'persons': persons})

def person(request, id):
	person = Person.objects.get(id=id)
	chapters_drawn = reversed(person.chapter_artists.all())
	chapters_written = reversed(person.chapter_writers.all())

	critic_score = person.get_critic_score()
	user_score = person.get_user_score()

	total_score = 'NR'
	if user_score == None and critic_score == None:
		user_score = 'NR'
		critic_score = 'NR'
	elif user_score == None:
		user_score = 'NR'
		total_score = critic_score
	elif critic_score == None:
		critic_score = 'NR'
		total_score = user_score
	else:
		total_score = person.get_total_score()

	return render(request, 'person.html', {'person': person, 'chapters_drawn': chapters_drawn, 'chapters_written': chapters_written, 'total_score': total_score, 'critic_score': critic_score, 'user_score': user_score})
