# from django.db import models
from djongo import models

# Create your models here.
class Person(models.Model):
	name = models.CharField(max_length=255)
	surname = models.CharField(max_length=255)
	birth_date = models.DateField(default=None)
	photo_link = models.CharField(max_length=255, default=None)

	def __str__(self):
		return self.name + ' ' + self.surname
		
	def get_total_score(self):
		return int((self.get_critic_score() + self.get_user_score()) / 2)

	def get_critic_score(self):
		chapters_drawn_critic_score = self.get_chapters_drawn_critic_score()
		chapters_written_critic_score = self.get_chapters_written_critic_score()

		critic_score = None
		if chapters_drawn_critic_score != None and chapters_written_critic_score != None:
			critic_score = int((len(self.chapter_artists.all()) * chapters_drawn_critic_score + len(self.chapter_writers.all()) * chapters_written_critic_score) / (len(self.chapter_artists.all()) + len(self.chapter_writers.all())))
		elif chapters_drawn_critic_score != None:
			critic_score = int(chapters_drawn_critic_score)
		elif chapters_written_critic_score != None:
			critic_score = int(chapters_written_critic_score)

		return critic_score

	def get_chapters_drawn_critic_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_artists.all()
		cumulated_score = 0
		for chapter in chapters:
			critic_score = chapter.get_critic_score()
			if critic_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += critic_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	def get_chapters_written_critic_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_writers.all()
		cumulated_score = 0
		for chapter in chapters:
			critic_score = chapter.get_critic_score()
			if critic_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += critic_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	def get_user_score(self):
		chapters_drawn_user_score = self.get_chapters_drawn_user_score()
		chapters_written_user_score = self.get_chapters_written_user_score()

		user_score = None
		if chapters_drawn_user_score != None and chapters_written_user_score != None:
			user_score = int((len(self.chapter_artists.all()) * chapters_drawn_user_score + len(self.chapter_writers.all()) * chapters_written_user_score) / (len(self.chapter_artists.all()) + len(self.chapter_writers.all())))
		elif chapters_drawn_user_score != None:
			user_score = int(chapters_drawn_user_score)
		elif chapters_written_user_score != None:
			user_score = int(chapters_written_user_score)

		return user_score

	def get_chapters_drawn_user_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_artists.all()
		cumulated_score = 0
		for chapter in chapters:
			user_score = chapter.get_user_score()
			if user_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += user_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	def get_chapters_written_user_score(self):
		not_rated_chapters_number = 0
		chapters = self.chapter_writers.all()
		cumulated_score = 0
		for chapter in chapters:
			user_score = chapter.get_user_score()
			if user_score == None:
				not_rated_chapters_number += 1
			else:
				cumulated_score += user_score

		if len(chapters) - not_rated_chapters_number > 0:
			score = cumulated_score / (len(chapters) - not_rated_chapters_number)
			return score

		return None

	@property
	def get_chapters_drawn_number(self):
		return len(self.chapter_artists.all())

	@property
	def get_chapters_written_number(self):
		return len(self.chapter_writers.all())
	